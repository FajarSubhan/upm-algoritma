(*
 * @Title       : Soal UPM No 1
 * @Matkul		: Algoritma
 * @CreatedBy	: Fajar Subhan
 * @Nim			: 202043500578
 * @License 	: The GNU General Public License (GNU GPL or simply GPL) https://www.gnu.org/
 * @Docs		: https://www.freepascal.org/docs.html
*)
program UPM_Satu;
uses crt;

var 
  a , b , c, d : integer;
  
begin 
clrscr;

a := 8; // Nilai NPM terakhir masing masing 
a := a + 1;
d := (14 + a * 2) div 2;
d := d - a;
b := 0;
c := 1;

{ d := 2; } // sample variable d 2 agar looping while tidak berlebihan


while(c <= d) do  
begin
  { Jika sisa hasil bagi dari variable d adalah 0 maka tambah variable b sebanyak + 1,
    tetapi perhatikan variable d nya harus bernilai 2 agar looping terjadi 2x saja dan 
	penjumlahan b hanya 2x yaitu b = 2
 }
  if(d mod 2 = 0) then 
    begin 
		b := b + 1;
	end;
  c := c + 1;  
end;

write(b);
write(d);

{*
 /----------------
 / @Tiban Variable b sehingga valuenya = 2 dan pasti nilai nya UNIVERSITAS
 / Karena jika variable d sisa hasil baginya tidak sama dengan 0 maka variable b pasti akan selalu 0 valuenya 
 /---------------
*}
{ b := 2;  }

if(b = 2) then 
  begin 
	write('UNIVERSITAS');
  end
else 
  begin 
	write('INDRAPRASTA');
  end;
end.